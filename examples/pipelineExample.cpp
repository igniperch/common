#include <iostream>

#define REQUIRES(...) class=std::enable_if_t<(__VA_ARGS__)>

template<class TForwarder>
struct CPipeClosure : TForwarder
{
   template<class ... TParams>
   CPipeClosure(TParams && ... params) : TForwarder(std::forward<TParams>(params)...)
   {}
};

template<class T, class TForwarder>
decltype(auto) operator | (T && t, const CPipeClosure<TForwarder> & forwarder)
{
   return forwarder(std::forward<T>(t));
};

struct increment
{
   template<class T>
   auto operator()(T t) const
   {
      return ++t;
   }
};

template<class TForwarder>
auto makePipeForwarder(TForwarder forwarder)
{
   return CPipeClosure<TForwarder>(std::move<forwarder>);
}

template<class T>
struct CWrapper
{
   T mValue;
   template<class TVariable, REQUIRES(std::is_convertible<T, TVariable>())>
   CWrapper(TVariable && variable) : mValue(std::forward<TVariable>(variable))
   {}

   T get() const
   {
      return std::move(mValue);
   }
};

template<class T>
auto makeWrapper(T && t)
{
   return CWrapper<T>(std::forward<T>(t));
}

template<class TForwarder>
struct CPipable
{
   template<class... Args>
   auto operator()(Args &&... args) const
   {
      return makePipeForwarder([](auto... wrappedArgs)
      {
         return [=](auto && value) -> decltype(auto)
         {
            return TForwarder()(value, wrappedArgs.get()...);
         };
      }(makeWrapper(std::forward<Args>(args)...)));
    }
};

template<class T, class TForwarder>
decltype(auto) operator|(T && value, const CPipable<TForwarder> & pipable)
{
    return TForwarder()(std::forward<T>(value));
}

//using spec = add_reference<add_const<add_pointer<X>::type>>;

int main()
{
   std::cout << "Hello canary" << std::endl;
   const constexpr CPipable<increment> inc = {};
   int result = 1 | inc | inc;
   std::cout << result << std::endl; // Should be 3)
   return 0;
}
