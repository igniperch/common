/*!
   \file ILogger.hpp
   \brief Interface class for logging.
*/
#ifndef COMMON__LOGGER_ILOGGER_HPP
#define COMMON__LOGGER_ILOGGER_HPP
#include <string>
#include <mutex>
#include "CommonTypes.hpp"

namespace NLogger
{
/*!
 * \brief Interface for logger class.
 * 
 */
class ILogger
{
public:
   //! Deleted construnctor. 
   ILogger() = delete;
   //! Empty constructor. If someone planned use his own instantination of logger. I don't know why.
   ILogger(const LogConfig &){};
   //! Virtual destructor. 
   virtual ~ILogger(){};

   //! Interface method to log message with custom log level.
   virtual void log(const std::string &, const ELogLevel) = 0;
   //! Interface method to log message with default global log level.
   virtual void log(const std::string &) = 0;
protected:
   //! Mutex for thread safety. 
   std::mutex mLock;
};
} // namespace NLogger

#endif // COMMON__LOGGER_ILOGGER_HPP