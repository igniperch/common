/*!
   \file CommonTypes.hpp
   \brief Common types for logger.
*/
#ifndef COMMON__LOGGER_COMMONTYPES_HPP
#define COMMON__LOGGER_COMMONTYPES_HPP

#include <string>
#include <unordered_map>

namespace NLogger
{
/*!
 * \brief Enum hasher. 
 * 
 * Usage:
 * \code
      using LogTags = std::unordered_map<ELogLevel, std::string, CEnumHasher>;
 * \endcode
 */
struct CEnumHasher 
{ 
   /*!
   * \brief Calculate size of param.
   *
   * \sa std::size_t
   */
   template <typename T> 
   std::size_t operator () (T t) const 
   { 
      return static_cast<std::size_t>(t); 
   } 
};

/*!
 * \brief Source info.  
 * 
 */
struct CSourceInfo
{
   //! Name of source file.
   std::string mFileName;
   //! Line of code.
   int32_t mLine;
   //! Method name.
   std::string mFunction;
};

/*!
 * \brief Log levels. 
 * 
 */
enum class ELogLevel : uint8_t
{
   //! Trace level. The lowest priority. Can be used for a custom logger. 
   TRACE = 0,
   //! Debug level. Can be used for separate debug and release version.
   DEBUG = 1,
   //! Information level. Most of messages will be logged as INFO.
   INFO = 2,
   //! Warning level.
   WARNING = 3,
   //! Error level. The highest priority.
   ERROR = 4
};

/*!
 * \brief Log tags type. 
 * 
 * Describes LogLevels of tags, tag names and type hasher.
 */
using LogTags = std::unordered_map<ELogLevel, std::string, CEnumHasher>;

//! Map for uncolored tags. TODO: add short and long prefix.
const LogTags uncoloredTags
{
   {ELogLevel::ERROR, " [ERR] "}, {ELogLevel::WARNING, " [WRN] "}, 
   {ELogLevel::INFO, " [INF] "}, {ELogLevel::DEBUG, " [DBG] "}, 
   {ELogLevel::TRACE, " [TRC] "}
};

//! Map for colored tags. TODO: add short and long prefix.
const LogTags coloredTags
{
   {ELogLevel::ERROR, " \x1b[31;1m[ERR]\x1b[0m "}, {ELogLevel::WARNING, " \x1b[33;1m[WRN]\x1b[0m "}, 
   {ELogLevel::INFO, " \x1b[32;1m[INF]\x1b[0m "}, {ELogLevel::DEBUG, " \x1b[34;1m[DBG]\x1b[0m "}, 
   {ELogLevel::TRACE, " \x1b[37;1m[TRC]\x1b[0m "} 
};

/*!
* \brief Logger configuration. 
* Log config can be setted in project with method void NLogger::configure(const LogConfig & logConfig).
*
* This is a key-value string map. Possible set:
* \code
   NLogger::configure({ {"type", "std_out"} {"colored", ""} });
* \endcode
*
* A value of standalone key can be setted as empty string: ""
*/
using LogConfig = std::unordered_map<std::string, std::string>;

} // namespace NLogger
#endif // COMMON__LOGGER_COMMONTYPES_HPP