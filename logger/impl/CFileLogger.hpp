/*!
   \file CFileLogger.hpp
   \brief Class for logging to file.
*/
#ifndef COMMON__LOGGER_CFILELOGGER_HPP
#define COMMON__LOGGER_CFILELOGGER_HPP

#include "CommonTypes.hpp"
#include "Constrants.hpp"
#include "StringUtils.hpp"
#include "ILogger.hpp"

#include <fstream>

namespace NLogger
{
/*!
 * \brief Class for loggering to file;
 * 
 * TODO: Need to add packaged logging. 
 */
class CFileLogger : public ILogger
{
public:
   //! Deleted cosntructor. Logger without params can't be created.
   CFileLogger() = delete;
   /*!
   * \brief Initializing constructor. 
   * 
   * \param Logger configuration. 
   */
   CFileLogger(const LogConfig & logConfig)
   : ILogger(logConfig)
   {
      //! Filename must be setted in params.
      auto fileName = logConfig.find("file_name");
      if(fileName == logConfig.end())
      {
         //! TODO: Add exeption-free realization.
         throw std::runtime_error("No output file provided.");
      }

      //! File must been reopened after reopen interval.
      mFileName = fileName->second;
      auto settedReopenInterval = logConfig.find("reopen_interval");
      if(settedReopenInterval == logConfig.end())
      {
         try
         {
            mReopenInterval = std::chrono::seconds(std::stoul(settedReopenInterval->second));
         }
         catch(...)
         {
            throw std::runtime_error(settedReopenInterval->second + " is not a valid reopen interval");
         }
      }
      else
      {
         mReopenInterval = REOPEN_FILE_INTERVAL;
      }
      
      reopen();
   }

   /*!
   * \brief Real logging method with the local level of logging. 
   * 
   * TODO: need to refactor with CSTDLogger because reusable code detected. 
   */
   virtual void log(
      //! \param Logged message
      const std::string & message, 
      //! \param Log level.
      const ELogLevel logLevel) override
   {
      //! Log level mustn't be lower than global log level.
      if(logLevel < GLOBAL_LOG_LEVEL)
      {
         return;
      }
      //! Form output message.
      std::string outputMessage;
      outputMessage.reserve(message.length() + MESSAGE_PREFIX_LENGTH);
      outputMessage.append(NStringUtils::timestamp());
      outputMessage.append(uncoloredTags.find(logLevel)->second);
      outputMessage.append(message);
      outputMessage.push_back('\n');
      log(outputMessage);
   }

   /*!
   * \brief Real logging method. 
   * 
   * \param Message for logging.
   */
   virtual void log(const std::string & message)
   {
      std::lock_guard<std::mutex> lock{mLock};
      mFile << message;
      //! Workaround for case when file was modified from other place.
      mFile.flush();
      reopen();
   }
protected:
   //! Reopen method. Trying to reopen file after some time..
   void reopen()
   {
      //! Using std::lock_guard for thread-safety.
      std::lock_guard<std::mutex> lock{mLock};

      //! Calculate if time from the last opening is enough.
      auto now = std::chrono::system_clock::now();
      if((now - mLastReopen) > mReopenInterval)
      {
         mLastReopen = now;
         //! Trying to close file. Nothing to do if not (|(-_-)|)
         try
         {
            mFile.close();
         }
         catch(...)
         {
            //! TODO: redesign
         }

         //! Trying to open file. TODO: redesign to use logger without any exceptions.
         try
         {
            mFile.open(mFileName, std::ofstream::out | std::ofstream::app);
            mLastReopen = std::chrono::system_clock::now();
         }
         catch(std::exception & e)
         {
            throw e;
         }
      }
   }
protected:
   //! Output file name. 
   std::string mFileName;
   //! File object.
   std::ofstream mFile;
   //! Local reopen interval. 
   std::chrono::seconds mReopenInterval;
   //! Point in time when file was opened.
   std::chrono::system_clock::time_point mLastReopen;
};


} // namespace NLogger

#endif // COMMON__LOGGER_CFILELOGGER_HPP