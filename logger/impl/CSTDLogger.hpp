/*!
   \file CSTDLogger.hpp
   \brief Class for logging to std::cout.
*/
#ifndef COMMON__LOGGER_CSTDLOGGER_HPP
#define COMMON__LOGGER_CSTDLOGGER_HPP

#include "CommonTypes.hpp"
#include "Constrants.hpp"
#include "StringUtils.hpp"
#include "ILogger.hpp"

#include <iostream>

namespace NLogger
{
/*!
 * \brief Class for loggering to std::cout;
 * 
 * TODO: 
 *  1. Need to modify it for logging to abstract std:stream. 
 *  2. Need to add packaged logging. 
 */
class CSTDLogger : public ILogger
{
public:
   //! Deleted contstructor.
   CSTDLogger() = delete;
   /*!
   * \brief Initializing constructor. 
   * 
   * \param Logger configuration. 
   */
   CSTDLogger(const LogConfig & logConfig)
   : ILogger(logConfig)
   , mLogTags(logConfig.find("color") != logConfig.end() ? coloredTags : uncoloredTags)
   {
   }

   /*!
   * \brief Real logging method with the local level of logging. 
   * 
   */
   virtual void log(
      //! \param Logged message.
      const std::string & message, 
      //! \param Log level.
      const ELogLevel logLevel) override
   {
      //! We don't need to log message with too low log level.
      if(logLevel < GLOBAL_LOG_LEVEL)
      {
         return;
      }
      //! Form and log output message with loglevel tag.
      std::string outputMessage;
      outputMessage.reserve(message.length() + MESSAGE_PREFIX_LENGTH);
      outputMessage.append(NStringUtils::timestamp());
      outputMessage.append(mLogTags.find(logLevel)->second);
      outputMessage.append(message);
      outputMessage.append("\n");
      log(outputMessage);
   }

   /*!
   * \brief Real logging method. 
   * 
   * \param Message for logging.
   */
   virtual void log(const std::string & message) override
   {
      std::cout << message;
      // Need to flush level
      std::cout.flush();
   }

protected:
   //! Tags with log level. For example: WARNING, ERROR.
   const LogTags mLogTags;
};

} // namespace NLogger

#endif // COMMON__LOGGER_CSTDLOGGER_HPP