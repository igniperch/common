/*!
   \file impl/StringUtils.hpp
   \brief Utils for formating messages.
*/
#ifndef SOURCE_M2P2H_STRINGUTILS
#define SOURCE_M2P2H_STRINGUTILS
#include <string>
#include <vector>
#include <chrono>
#include <cctype>
#include <ctime>
#include <utility>
#include <type_traits>
#include <thread>
#include <sstream>

#include "Constrants.hpp"

namespace
{

/*!
* \bref Emplace params into the message.
*
* \param message
* \param vector of params witch will be emplaced into the message.
* \return formatted string.
* \note Magic  ¯\_(ツ)_/¯
*/
std::string formatterImpl(const std::string & message, const std::vector<std::string> & strs)
{
   std::string resultString;
   std::string buffer;
   bool tagFound = false;
   int formatterSize = static_cast<int>(message.size());
   //! Iterate input string for %number "tag". For example "Message and %1 message".
   for(int i = 0; i <= formatterSize; ++i)
   {
      bool isLast = i == formatterSize;
      char symbol = message[i];
      //! Tag was found before.
      if(tagFound)
      {
         //! Add symbol to buffer if it digit. For example %123
         if(std::isdigit(static_cast<unsigned char>(symbol)))
         {
            buffer += symbol;
         }
         else
         {
            //! This is not a digit symbol. Trying to emplace tag with param from vector.
            int number = 0;
            if(!buffer.empty())
            {
               number = std::stoi(buffer);
            }
            //! Need to check is number in tag not higher than input params.
            if(number >= 1 && number <= static_cast<int>(strs.size()))
            {
               //! Add param to output string.
               resultString += strs[number - 1];
            }
            else
            {
               //! Nothing to do. Add tag to result as is. 
               resultString += NLogger::MESSAGE_PARAMS_DELIMER + buffer;
            }
            // Looking for a new tag. For example it can be Message=[%1%2].
            buffer.clear();
            if(symbol != NLogger::MESSAGE_PARAMS_DELIMER)
            {
               if(!isLast)
               {
                  resultString += symbol;
               }
               tagFound = false;
            }
         }
      }
      else
      {
         // Trying to detect delimer in symbol.
         if(symbol == NLogger::MESSAGE_PARAMS_DELIMER)
         {
            tagFound = true;
         }
         else
         {
            if(!isLast)
            {
               resultString += symbol;
            }
         }
         
      }
   }
   return resultString;
}

/*!
* \bref Template method for cast type to std::string
*
* \param value witch will be converted.
* \return Converted into the std::string value.
*
* \note This method can't be called on std::string and char* and std::thread::id
*/
template<typename T> 
inline std::string to_string(const T &t)
{
   return std::to_string(t);
}

/*!
* \bref Method to convert char* to std::string
*
* \param char* value witch will be converted.
* \return Converted into the std::string value.
*
* \note This method needed because of std::to_string doesn't work with char*.
*/
std::string to_string(const char *t)
{ 
   return std::string(t); 
}

/*!
* \bref Templace instance convert std::thread::id into the std::string.
*
* \param input threadID
* \return iConverted into the std::string value.
*
* \note This instance needed to use std::string toString(const T & t) with std::thread::id. 
*/
template<>
std::string to_string(const std::thread::id & t)
{ 
   std::stringstream ss;
   ss << t;
   return ss.str(); 
}

/*!
* \bref Templace instance return value.
*
* \param input std::string
* \return input value.
*
* \note This instance needed to use std::string toString(const T & t) with strings. 
*/
template<> 
inline std::string to_string<std::string>(const std::string &t)
{
   return t;
}

/*!
* \bref Implementation of formatting template.
*
* \param input std::string
* \return input value.
*
* \note This instance needed to use std::string toString(const T & t) with strings. 
*/
template<typename Arg, typename ... Args> 
inline std::string formatterImpl(const std::string & message, std::vector<std::string> & strs, Arg && arg, Args && ... args)
{
   strs.push_back(to_string(std::forward<Arg>(arg)));
   return formatterImpl(message, strs, std::forward<Args>(args)...);
}

} // anonymous namespace

namespace NLogger
{
namespace NStringUtils
{

/*! 
   \bref Formatter for case when input string without parameters. 
   \note Nothing to do. Just return input string.

   \param Input string.
   \return Input string as is. 
*/
inline std::string formatter(const std::string & message)
{
   return message;
}

/*!
* \bref Formatter to emplace parameters into input string.
* \param Input string.
* \param List of params different types. 
*
* \note Supported parameter types: basics (int, double, std::string etc) and std::thread::id.
*
* Usage:
* \code
*   std::cout << NStringUtils::formatter("Formatter example %1 + %2 = %3", 2, 3, 2+3) << std::endl;
*   std::cout << NStringUtils::formatter("Formatter mixed types %1  %2  %3 true=%4", 2, 3, 3.4, false) << std::endl;
*   std::string ss = "std::string";
*   std::cout << NStringUtils::formatter("Formatter mixed string types %1  %2  %3 string=%4 char*=%5", 2, 3, 3.4, ss, "CHAR*") << std::endl;
* \exitcode
*/
template<typename Arg, typename ... Args>
inline std::string formatter(const std::string & message, Arg && arg, Args && ... args)
{
   std::vector<std::string> strs;
   return formatterImpl(message, strs, std::forward<Arg>(arg), std::forward<Args>(args)...);
}

/*! 
   \bref Make string with information about source file, method and line of code.

   \param Input message.
   \param Source file name.
   \param Method where log methods was called.
   \param Line in code where log method was called.
   \return Formatted string. 

   \note TODO: Need to add configuration of output format. Now it's [file:line][method]message
*/
template<typename T>
std::string appendFileInfo(const T & message, const std::string & file, const std::string & method, const int line)
{
   std::stringstream ss;
   ss << "[" 
      << file
      << ":"
      << line
      << "] ["
      << method
      << "] "
      << message;
   return ss.str();
}

/*! 
* \bref Strip result of __PRETTY_FUNCTION___ call. 
* 
* \param Input string with __PRETTY_FUNCTION__ call result.
* \return Stripped call method string with parents. 
*
* Example: 
* Input string: virtual void NFoo::COuter::printOut(const string&)
* Output string: NFoo::COuter::printOut
*/
template<typename T>
std::string stripPrettyFunction(const T & pf)
{
   std::string prettyFunction = std::string(pf);
   auto stopBit = prettyFunction.find_last_of("(");
   std::string strippedRight = prettyFunction.substr(0, stopBit);
   auto startBit = strippedRight.find_last_of(" ");
   return strippedRight.substr(startBit + 1, stopBit);
}

//! Calculate current timestamp into the std::string.
inline std::string timestamp()
{
   //! Get local time.
   std::chrono::system_clock::time_point timePoint =  std::chrono::system_clock::now();
   std::time_t localTime = std::chrono::system_clock::to_time_t(timePoint);

   //! Get GMT-time.
   std::tm gmt{};
   gmtime_r(&localTime, &gmt);

   //! Get duration in seconds. 
   std::chrono::duration<double> durationSeconds = (timePoint - std::chrono::system_clock::from_time_t(localTime)) + std::chrono::seconds(gmt.tm_sec);

   //! Format string.
   std::string stringBuffer("yyyy/mm/dd hh:mm:ss.xxxxxx");
   sprintf(&stringBuffer.front(), "%04d/%02d/%02d %02d:%02d:%09.6f", gmt.tm_year + 1900, gmt.tm_mon + 1,
                                                                     gmt.tm_mday, gmt.tm_hour, gmt.tm_min, durationSeconds.count());

   return stringBuffer;
}

} // NStringUtils
} // Nm2p2h
#endif