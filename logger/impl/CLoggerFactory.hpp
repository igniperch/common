/*!
   \file CLoggerFactory.hpp
   \brief Concrete logger generation supplier.
*/
#ifndef COMMON__LOGGER_CLOGGERFACTORY_HPP
#define COMMON__LOGGER_CLOGGERFACTORY_HPP
#include <string>
#include <memory>
#include <functional>
#include "CFileLogger.hpp"
#include "CSTDLogger.hpp"

namespace NLogger
{
//! Typename for the abstract logger creator. 
using LoggerCreator = std::function<std::unique_ptr<ILogger>(const LogConfig & logConfig)>;

/*!
 * \brief Factory for creating concrete logger.
 * 
 */
class CLoggerFactory
{
public:
   //! Factory constructor. Logger creators for every possible loggers must emplaced here.
   CLoggerFactory()
   {
      mCreators.emplace("std_out",
                        [](const LogConfig & logConfig)
                        {
                           return std::make_unique<CSTDLogger>(logConfig);
                        });

      mCreators.emplace("file_out",
                        [](const LogConfig & logConfig)
                        {
                           return std::make_unique<CFileLogger>(logConfig);
                        });
   }

   //! Create concrete logger. 
   std::unique_ptr<ILogger> produce(const LogConfig & logConfig)
   {
      auto logType = logConfig.find("type");
      if(logType == logConfig.end())
      {
         throw std::runtime_error("Unexpected type of logging configuration");
      }

      auto creator = mCreators.find(logType->second);
      if(creator == mCreators.end())
      {
         throw std::runtime_error("Couldn't produce logger for type: " + logType->second);
      }

      return creator->second(logConfig);
   }
protected:
   //! Possible creators of concrete logger.
   std::unordered_map<std::string, LoggerCreator> mCreators;
};

//! Empty creator.
inline CLoggerFactory & getFactory()
{
   static CLoggerFactory staticFactory{};
   return staticFactory;
}

/*! 
* \bref Static creator for logger.
*
* \param Logger configuration.
* \return pointer to logger object.
*/
inline ILogger & getLogger(const LogConfig & logConfig = { {"type", "std_out"}, {"color", ""}, {"source", ""}, {"method", ""}})
{
   static std::unique_ptr<ILogger> staticLogger(getFactory().produce(logConfig));
   return *staticLogger;
}

} // namespace NLogger

#endif // COMMON__LOGGER_CLOGGERFACTORY_HPP