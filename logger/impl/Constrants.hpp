/*!
   \file Constrants.hpp
   \brief Various constrants for Logger. 
*/
#ifndef COMMON__LOGGER_CONSTRANTS_HPP
#define COMMON__LOGGER_CONSTRANTS_HPP

#include <chrono>
#include "CommonTypes.hpp"

namespace NLogger
{
//! Max message prefix length. TODO: Must be configured?
constexpr size_t MESSAGE_PREFIX_LENGTH = 60;

//! Reopen file interval. Used for the file consistency checking. Can be reconfigurated in Log config.
constexpr std::chrono::seconds REOPEN_FILE_INTERVAL = std::chrono::seconds(300);

//! Delimer for emplacing params into the message. For example: Message with param1=%1 param2=%2.
constexpr char MESSAGE_PARAMS_DELIMER = '%';

//! Checking log-level. Default log level: info.
#if defined(LOGGING_LEVEL_ALL) || defined(LOGGING_LEVEL_TRACE)
   constexpr ELogLevel GLOBAL_LOG_LEVEL = ELogLevel::TRACE;
#elif defined(LOGGING_LEVEL_DEBUG)
   constexpr ELogLevel GLOBAL_LOG_LEVEL = ELogLevel::DEBUG;
#elif defined(LOGGING_LEVEL_WARN)
   constexpr ELogLevel GLOBAL_LOG_LEVEL = ELogLevel::WARN;
#elif defined(LOGGING_LEVEL_ERROR)
   constexpr ELogLevel GLOBAL_LOG_LEVEL = ELogLevel::ERROR;
#elif defined(LOGGING_LEVEL_NONE)
   constexpr ELogLevel GLOBAL_LOG_LEVEL = ELogLevel::ERROR + 1;
#else
   constexpr ELogLevel GLOBAL_LOG_LEVEL = ELogLevel::INFO;
#endif
}

#endif // COMMON__LOGGER_CONSTRANTS_HPP