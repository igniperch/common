#ifndef SOURCE_M2P2H_CLOGGER
#define SOURCE_M2P2H_CLOGGER

#include <cstring>

#define LOGGING_LEVEL_ALL
#include "impl/StringUtils.hpp"
#include "impl/CLoggerFactory.hpp"

namespace 
{
using namespace NLogger;

/*!
* \bref Implementation of call log TRACE message.
*
* \param Message for logging.
* \param Arguments of message which will be emplaced inside it.
*/
template<typename ... Args>
inline void TRACEImpl(const std::string & message, const Args ... args)
{
   getLogger().log(NStringUtils::formatter(message, args...), ELogLevel::TRACE);
}

/*!
* \bref Implementation of call log DEBUG message.
*
* \param Message for logging.
* \param Arguments of message which will be emplaced inside it.
*/
template<typename ... Args>
inline void DEBUGImpl(const std::string & message, const Args ... args)
{
   getLogger().log(NStringUtils::formatter(message, args...), ELogLevel::DEBUG);
}

/*!
* \bref Implementation of call log ERROR message.
*
* \param Message for logging.
* \param Arguments of message which will be emplaced inside it.
*/
template<typename ... Args>
inline void ERRORImpl(const std::string & message, const Args ... args)
{
   getLogger().log(NStringUtils::formatter(message, args...), ELogLevel::ERROR);
}

/*!
* \bref Implementation of call log INFO message.
*
* \param Message for logging.
* \param Arguments of message which will be emplaced inside it.
*/
template<typename ... Args>
inline void INFOImpl(const std::string & message, const Args ... args)
{
   getLogger().log(NStringUtils::formatter(message, args...), ELogLevel::INFO);
}

/*!
* \bref Implementation of call log WARNING message.
*
* \param Message for logging.
* \param Arguments of message which will be emplaced inside it.
*/
template<typename ... Args>
inline void WARNINGImpl(const std::string & message, const Args ... args)
{
   getLogger().log(NStringUtils::formatter(message, args...), ELogLevel::WARNING);
}

} // anonymous namespace 

//! Macros for calculating filename without path. TODO: move to string utils and make normal method.
#define __FILENAME__ (std::strrchr(__FILE__, '/') ? std::strrchr(__FILE__, '/') + 1 : __FILE__)

namespace NLogger
{
/*! 
* \bref Configurate logging.
* \note This method must be called once. 
*
* \param Logger configuration. See impl/CommonTypes.hpp .
* Example of usage:
* \code
*     NLogger::configure({ {"type", "file"}, {"file_name", "test.log"}, {"reopen_interval", "1"} });
* \exitcode
*/
inline void configure(const LogConfig & logConfig)
{
   getLogger(logConfig);
}

/*! 
* \bref Custom log message with args.
* This message don't use log level.
*
* \param Message for logging.
* \param Arguments witch will be emplaced into message.
*
* Example of usage:
* \code
*     NLogger::log("Message text param1=[%1] param2=[%2] ThreadID=[%3]", 1, "stringParam", std::this_thread::get_id());
*
*     Output: Message text param1=[1] param2=[stringParam] ThreadID=[140426453894976]
* \exitcode
*/
template<typename ... Args>
inline void log(const std::string & message, const Args ... args)
{
   getLogger().log(NStringUtils::formatter(message, args...));
}

/*! 
* \bref Custom log message with args to setted log level.
* This message sends message with LogLevel tag.
*
* \param Message for logging.
* \param Logging level. Please see impl/CommonTypes.hpp.
* \param Arguments witch will be emplaced into message.
*
* Default output format: yyyy/mm/dd hh:mm:ss.dddddd [LVL] [source_file:line] [namespace::class::method] Message with various params.
* Example of usage:
* \code
*     NLogger::log("Message text param1=[%1] param2=[%2] ThreadID=[%3]", 1, "stringParam", std::this_thread::get_id());
*     
*     Output: 2019/09/10 13:31:26.580942 [INF] Message text param1=[1] param2=[stringParam] ThreadID=[140201052665664]
* \exitcode
*/
template<typename ... Args>
inline void log(const std::string & message, const ELogLevel logLevel, const Args ... args)
{
   getLogger().log(NStringUtils::formatter(message, args...), logLevel);
}

/*! 
* \bref Log message as TRACE message.
*
* \note Can be unprinted to logger in case of too high log level.
*
* \param Message for logging.
* \param Arguments witch will be emplaced into message.
*
* Default output format: yyyy/mm/dd hh:mm:ss.dddddd [LVL] [source_file:line] [namespace::class::method] Message with various params.
* Example of usage:
* \code
*     TRACE("Message text param1=[%1] param2=[%2] ThreadID=[%3]", 1, "stringParam", std::this_thread::get_id());
*     
*     2019/09/10 13:34:43.173297 [TRC] [main.cpp:31] [main] Message text param1=[1] param2=[stringParam] ThreadID=[139948954863424]
* \exitcode
*/
#define TRACE(message, ...) \
         TRACEImpl(NLogger::NStringUtils::appendFileInfo(message \
                                                      , __FILENAME__ \
                                                      , NLogger::NStringUtils::stripPrettyFunction(__PRETTY_FUNCTION__) \
                                                      ,__LINE__) \
                  ,##__VA_ARGS__)

/*! 
* \bref Log message as DEBUG message.
*
* \note Can be unprinted to logger in case of too high log level.
*
* \param Message for logging.
* \param Arguments witch will be emplaced into message.
*
* Default output format: yyyy/mm/dd hh:mm:ss.dddddd [LVL] [source_file:line] [namespace::class::method] Message with various params.
* Example of usage:
* \code
*     DEBUG("Message text param1=[%1] param2=[%2] ThreadID=[%3]", 1, "stringParam", std::this_thread::get_id());
*     
*     2019/09/10 13:35:30.581012 [DBG] [main.cpp:32] [main] Message text param1=[1] param2=[stringParam] ThreadID=[140432697644864]
* \exitcode
*/
#define DEBUG(message, ...) \
         DEBUGImpl(NLogger::NStringUtils::appendFileInfo(message \
                                                      , __FILENAME__ \
                                                      , NLogger::NStringUtils::stripPrettyFunction(__PRETTY_FUNCTION__) \
                                                      ,__LINE__) \
                  ,##__VA_ARGS__)

/*! 
* \bref Log message as ERROR message.
*
* \note Can be unprinted to logger in case of too high log level.
*
* \param Message for logging.
* \param Arguments witch will be emplaced into message.
*
* Default output format: yyyy/mm/dd hh:mm:ss.dddddd [LVL] [source_file:line] [namespace::class::method] Message with various params.
* Example of usage:
* \code
*     ERROR("Message text param1=[%1] param2=[%2] ThreadID=[%3]", 1, "stringParam", std::this_thread::get_id());
*     
*     2019/09/10 13:36:58.973223 [ERR] [main.cpp:33] [main] Message text param1=[1] param2=[stringParam] ThreadID=[140538039826240]
* \exitcode
*/
#define ERROR(message, ...) \
         ERRORImpl(NLogger::NStringUtils::appendFileInfo(message \
                                                      , __FILENAME__ \
                                                      , NLogger::NStringUtils::stripPrettyFunction(__PRETTY_FUNCTION__) \
                                                      ,__LINE__) \
                  ,##__VA_ARGS__)

/*! 
* \bref Log message as WARNING message.
*
* \note Can be unprinted to logger in case of too high log level.
*
* \param Message for logging.
* \param Arguments witch will be emplaced into message.
*
* Default output format: yyyy/mm/dd hh:mm:ss.dddddd [LVL] [source_file:line] [namespace::class::method] Message with various params.
* Example of usage:
* \code
*     WARNING("Message text param1=[%1] param2=[%2] ThreadID=[%3]", 1, "stringParam", std::this_thread::get_id());
*     
*     2019/09/10 13:38:09.341257 [WRN] [main.cpp:34] [main] Message text param1=[1] param2=[stringParam] ThreadID=[139645196511040]
* \exitcode
*/
#define WARNING(message, ...) \
         WARNINGImpl(NLogger::NStringUtils::appendFileInfo(message \
                                                      , __FILENAME__ \
                                                      , NLogger::NStringUtils::stripPrettyFunction(__PRETTY_FUNCTION__) \
                                                      ,__LINE__) \
                     ,##__VA_ARGS__)

/*! 
* \bref Log message as INFO message.
*
* \note Can be unprinted to logger in case of too high log level.
*
* \param Message for logging.
* \param Arguments witch will be emplaced into message.
*
* Default output format: yyyy/mm/dd hh:mm:ss.dddddd [LVL] [source_file:line] [namespace::class::method] Message with various params.
* Example of usage:
* \code
*     INFO("Message text param1=[%1] param2=[%2] ThreadID=[%3]", 1, "stringParam", std::this_thread::get_id());
*     
*     2019/09/10 13:39:30.325434 [INF] [main.cpp:36] [main] Message text param1=[1] param2=[stringParam] ThreadID=[140379897849664]
* \exitcode
*/
#define INFO(message, ...) \
         INFOImpl(NLogger::NStringUtils::appendFileInfo(message \
                                                      , __FILENAME__ \
                                                      , NLogger::NStringUtils::stripPrettyFunction(__PRETTY_FUNCTION__) \
                                                      ,__LINE__) \
                  ,##__VA_ARGS__)

} // namespace NLogger
#endif